package com.pandu.resepmasakan;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MasakanActivity extends AppCompatActivity {

    String daerah = "";
    ImageView fotonya;
    TextView tvDaerah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_masakan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fotonya = findViewById(R.id.imgMasakan);
        tvDaerah = findViewById(R.id.tv_nama_daerah);

        daerah = getIntent().getStringExtra("daerah");

        if (daerah.equals("padang")) {
            fotonya.setImageResource(R.drawable.masakan_padang);
            tvDaerah.setText("Masakannya orang padang");
        } else if (daerah.equals("jawa")) {
            fotonya.setImageResource(R.drawable.masakan_jawa);
            tvDaerah.setText("Orang jawa yang masak");
        } else {
            fotonya.setImageResource(R.drawable.masakan_sunda);
            tvDaerah.setText("Sunda masakannya");
        }
    }
}
