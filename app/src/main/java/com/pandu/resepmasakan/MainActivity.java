package com.pandu.resepmasakan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnPadang;
    Button btnJawa;
    Button btnSunda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPadang = findViewById(R.id.btn_padang);
        btnJawa = findViewById(R.id.btn_jawa);
        btnSunda = findViewById(R.id.btn_sunda);

        btnPadang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent padangIntent = new Intent(MainActivity.this, MasakanActivity.class);
                padangIntent.putExtra("daerah", "padang");
                startActivity(padangIntent);
            }
        });

        btnJawa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent jawaIntent = new Intent(MainActivity.this, MasakanActivity.class);
                jawaIntent.putExtra("daerah", "jawa");
                startActivity(jawaIntent);
            }
        });

        btnSunda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sundaIntent = new Intent(MainActivity.this, MasakanActivity.class);
                sundaIntent.putExtra("daerah", "sunda");
                startActivity(sundaIntent);
            }
        });

    }
}
